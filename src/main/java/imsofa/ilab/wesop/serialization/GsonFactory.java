/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import imsofa.ilab.wesop.model.FilePlayItem;
import imsofa.ilab.wesop.model.PlayItem;

/**
 *
 * @author lendle
 */
public class GsonFactory {
    public static Gson newGson(){
        GsonBuilder builder=new GsonBuilder();
        builder.setPrettyPrinting();
        builder.registerTypeAdapter(PlayItem.class, new PlayItemSerializer());
        builder.registerTypeAdapter(PlayItem.class, new PlayItemDeserializer());
        builder.registerTypeAdapter(FilePlayItem.class, new PlayItemSerializer());
        builder.registerTypeAdapter(FilePlayItem.class, new PlayItemDeserializer());
        return builder.create();
    }
}
