/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.model;

import java.util.Objects;

/**
 *
 * @author lendle
 */
public class QualifiedTask {
    private String projectName;
    private String taskName;

    public QualifiedTask(String projectName, String taskName) {
        this.projectName = projectName;
        this.taskName = taskName;
    }

    
    
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.projectName);
        hash = 67 * hash + Objects.hashCode(this.taskName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QualifiedTask other = (QualifiedTask) obj;
        if (!Objects.equals(this.projectName, other.projectName)) {
            return false;
        }
        if (!Objects.equals(this.taskName, other.taskName)) {
            return false;
        }
        return true;
    }
    
    
}
