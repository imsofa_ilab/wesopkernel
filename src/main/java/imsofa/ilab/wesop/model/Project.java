/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.model;

import com.google.gson.Gson;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class Project {

    private List<Task> tasks = null;
    private boolean enabled = true;
    private String name = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.tasks);
        hash = 89 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        
        if(this.tasks!=null && other.tasks!=null){
            List<Task> copiedList1=new ArrayList<>(this.tasks);
            List<Task> copiedList2=new ArrayList<>(other.tasks);
            Collections.sort(copiedList1, new Comparator<Task>(){

                @Override
                public int compare(Task o1, Task o2) {
                    return o1.getTaskId().compareTo(o2.getTaskId());
                }
            });
            
            Collections.sort(copiedList2, new Comparator<Task>(){

                @Override
                public int compare(Task o1, Task o2) {
                    return o1.getTaskId().compareTo(o2.getTaskId());
                }
            });
            
            if(!Objects.equals(copiedList1, copiedList2)){
                return false;
            }
        }
        
        if(this.tasks!=null && other.tasks==null){
            return false;
        }
        
        if(this.tasks==null && other.tasks!=null){
            return false;
        }
        
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}
