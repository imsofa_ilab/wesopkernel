/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class Player {
    private String id=null;
    private String ip=null;
    private int port=10001;
    private String displayingFile=null;
    private String currentDisplayedPart=null;
    private long lastViewed=-1;
    private PlayerStatus status=PlayerStatus.UNKNOWN;
    private int downloadingTotalParts=1;
    private int downloadingFinishedParts=0;
    private String name=null;
    private String groupId=null;
    private boolean enabled=true;
    private String sId=null;
    private boolean registered=false;
    private long syncTick=-1;
    private PlayerStatus pendingForStatus=null;//if not null, waiting for the specified status

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    
    
    public PlayerStatus getPendingForStatus() {
        return pendingForStatus;
    }

    public void setPendingForStatus(PlayerStatus pendingForStatus) {
        this.pendingForStatus = pendingForStatus;
    }
    
    //if the client appeared offline, we have to receive multiple
    //ack messages to make sure it is back
    private int awakeFromOfflineCount=0;
    private boolean offlineWhenRunning=false;

    public boolean isOfflineWhenRunning() {
        return offlineWhenRunning;
    }

    public void setOfflineWhenRunning(boolean offlineWhenRunning) {
        this.offlineWhenRunning = offlineWhenRunning;
    }
    
    

    public int getAwakeFromOfflineCount() {
        return awakeFromOfflineCount;
    }

    public void setAwakeFromOfflineCount(int awakeFromOfflineCount) {
        this.awakeFromOfflineCount = awakeFromOfflineCount;
    }   

    public long getSyncTick() {
        return syncTick;
    }

    public synchronized void setSyncTick(long syncTick) {
        this.syncTick = syncTick;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
    
    

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }
    
    

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public int getDownloadingTotalParts() {
        return downloadingTotalParts;
    }

    public void setDownloadingTotalParts(int downloadingTotalParts) {
        this.downloadingTotalParts = downloadingTotalParts;
    }

    public int getDownloadingFinishedParts() {
        return downloadingFinishedParts;
    }

    public void setDownloadingFinishedParts(int downloadingFinishedParts) {
        this.downloadingFinishedParts = downloadingFinishedParts;
    }

    
    
    public PlayerStatus getStatus() {
        return status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }
    
    

    public long getLastViewed() {
        return lastViewed;
    }

    public void setLastViewed(long lastViewed) {
        this.lastViewed = lastViewed;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDisplayingFile() {
        return displayingFile;
    }

    public void setDisplayingFile(String displayingFile) {
        this.displayingFile = displayingFile;
    }

    public String getCurrentDisplayedPart() {
        return currentDisplayedPart;
    }

    public void setCurrentDisplayedPart(String currentDisplayedPart) {
        this.currentDisplayedPart = currentDisplayedPart;
    }
    
    public Map<String, String> toConfig(){
        Map<String, String> map=new HashMap<>();
        map.put("id", this.id);
        map.put("name", this.name);
        map.put("groupId", this.groupId);
        map.put("sId", this.sId);
        map.put("enabled", ""+this.enabled);
        map.put("registered", ""+this.registered);
        return map;
    }
}
