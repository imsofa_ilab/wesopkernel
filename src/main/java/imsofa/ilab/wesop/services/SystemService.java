/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.services;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import imsofa.ilab.wesop.model.QualifiedTask;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class SystemService {

    private String host = "localhost";
    private int port = 8080;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Map getSystemState() throws MalformedURLException {
        String json = HttpRequest.get(new URL("http://" + host + ":" + port + "/systemState")).body();
        return new Gson().fromJson(json, Map.class);
    }

    public List<QualifiedTask> getRunningTasks() throws MalformedURLException {
        return this.getTasksFromQueue("runningTaskInstances");
    }
    
    public List<QualifiedTask> getFailedTasks() throws MalformedURLException {
        return this.getTasksFromQueue("failedTaskInstances");
    }
    
    public List<QualifiedTask> getTerminatingTasks() throws MalformedURLException {
        return this.getTasksFromQueue("terminatingTaskInstances");
    }
    
    public List<QualifiedTask> getTasksFromQueue(String queueName) throws MalformedURLException {
        Map systemState = this.getSystemState();
        List<Map> runningInstances = (List) ((Map) systemState.get("runningState")).get(queueName);
        Map<QualifiedTask, String> tasks=new HashMap<>();
        for(Map instance : runningInstances){
            QualifiedTask task=new QualifiedTask(""+instance.get("projectName"), ""+instance.get("taskName"));
            tasks.put(task, "");
        }
        return new ArrayList<QualifiedTask>(tasks.keySet());
    }
    
    public List<Map> getClients() throws MalformedURLException{
        String json = HttpRequest.get(new URL("http://" + host + ":" + port + "/clients")).body();
        return new Gson().fromJson(json, List.class);
    }
}
