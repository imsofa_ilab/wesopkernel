/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.services;

import com.github.kevinsawicki.http.HttpRequest;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class ProjectExecutor {
    private String host="localhost";
    private int port=8080;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    public void executeProjects(String projectName, String taskName, String taskPlayer) throws Exception{
        Map<String, String> params=new HashMap<>();
        if(projectName!=null){
            params.put("projectName", projectName);
        }
        if(taskName!=null){
            params.put("taskName", taskName);
        }
        if(taskPlayer!=null){
            params.put("taskPlayer", taskPlayer);
        }
        HttpRequest.post("http://"+host+":"+port+"/executeProjects", params, true).code();
        //new URL("http://"+host+":"+port+"/executeProjects?projectName="+projectName).openStream();
    }
    
    public void terminateProjects(String projectName, String taskName, String taskPlayer) throws Exception{
        Map<String, String> params=new HashMap<>();
        if(projectName!=null){
            params.put("projectName", projectName);
        }
        if(taskName!=null){
            params.put("taskName", taskName);
        }
        if(taskPlayer!=null){
            params.put("taskPlayer", taskPlayer);
        }
        HttpRequest.post("http://"+host+":"+port+"/terminateProjects", params, true).code();
        //new URL("http://"+host+":"+port+"/terminateProjects?projectName="+projectName).openStream();
    }
}
