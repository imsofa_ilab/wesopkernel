/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.wesop.factory;

import com.google.gson.Gson;
import imsofa.ilab.wesop.model.Project;
import imsofa.ilab.wesop.model.Task;
import imsofa.ilab.wesop.model.TaskDetail;
import imsofa.ilab.wesop.serialization.GsonFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author lendle
 */
public class ProjectFactory {
    
    public static List<Project> loadSampleProjects() throws IOException{
        URL url=ProjectFactory.class.getClassLoader().getResource("imsofa/ilab/wesop/model/SampleProjectList");
        //System.out.println("url="+url);
        String json=IOUtils.toString(url, "utf-8");
        return ProjectFactory.listFromJSON(json);
    }
    
    public static List<Project> loadProjects(String host, int port) throws IOException{
        URL url=new URL("http://"+host+":"+port+"/projects");
        String json=IOUtils.toString(url, "utf-8");
        return ProjectFactory.listFromJSON(json);
    }
    
    public static List<Project> loadProjects() throws IOException{
        return loadProjects("localhost", 8080);
    }
    
    public static Project fromJSON(String json) {
        Gson gson = GsonFactory.newGson();
        //Logger.getLogger(Project.class.getName()).info("json: " + json);
        Project project = gson.fromJson(json, Project.class);
        for (Task task : project.getTasks()) {
            task.setProject(project);
            for (TaskDetail detail : task.getTaskDetails()) {
                detail.setTask(task);
            }
        }
        return project;
    }
    
    public static List<Project> listFromJSON(String json) {
        try {
            Gson gson = GsonFactory.newGson();
            List list = gson.fromJson(json, List.class);
            List<Project> projects = new ArrayList<Project>();
            for (int i = 0; list != null && i < list.size(); i++) {
                Map map = (Map) list.get(i);
                projects.add(fromJSON(gson.toJson(map)));
            }

            return projects;
            //return GsonFactory.newGson().fromJson(json, List.class);
        } catch (Throwable e) {
            Logger.getLogger(Project.class.getName()).severe(e+":"+e.getMessage()+":"+e.getStackTrace()[0].toString());
            return null;
        }
    }
}
